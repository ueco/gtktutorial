#include <gtkmm.h>
#include <memory>

int main(int argc, char **argv){
    // Glib::RefPtr<Gtk::Application> app
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");

    Gtk::Window window;
    window.set_default_size(200, 200);

    return app->run(window);
}
